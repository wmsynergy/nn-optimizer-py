from DI.DIContainer import DIContainer
from DI.network_dependency import container_network_mapper
from DI.shared_dependency import container_shared_mapper
from DI.sequencer_dependency import container_sequencer_mapper
from Sequencer.Sequencer import Sequencer


container = DIContainer()
container.merge(container_network_mapper)
container.merge(container_shared_mapper)
container.merge(container_sequencer_mapper)

sequencer = container.get(Sequencer.__module__)
sequencer.run()
