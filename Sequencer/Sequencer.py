from DataStorage.DataStorage import DataStorage
from DI.DIContainer import DIContainer
from Network.Input.InputCollector import InputCollector
from Network.NetworkProcessor import NetworkProcessor
from Network.NetworkResultsCollector import NetworkResultsCollector


class Sequencer:
    __al_loopers: list[str]
    __fixed_loopers_number: int

    __processes = {
        'train': 'train_network',
        'test': '__propagate_test',
        'predict': '__propagate_prediction'
    }

    def __init__(
            self,
            data_storage: DataStorage,
            input_collector: InputCollector,
            network_processor: NetworkProcessor,
            network_results_collector: NetworkResultsCollector,
            container: DIContainer
    ):
        self.__data_storage: DataStorage = data_storage
        self.__input_collector: InputCollector = input_collector
        self.__network_processor: NetworkProcessor = network_processor
        self.__network_results_collector: NetworkResultsCollector = network_results_collector
        self.__container: DIContainer = container

    def run(self, process: str = 'train') -> None:

        self.__data_storage.set_process()

        getattr(self, self.__processes[process])()

    def train_network(self) -> None:

        self.__init_static_loopers()

        al_iterations: list[list[int]] = self.__data_storage.get_al_optimization_iterations()

        self.__network_results_collector.init()

        s00number: int = len(al_iterations[0])
        for s00 in range(s00number):  # from 0 to s00number-1
            i00: int = 0
            i00number: int = al_iterations[0][s00]
            while (self.__al_loopers[0]).set_settings(0, s00, i00, i00number) < i00number:

                i00 += 1

                self.__init_dynamic_loopers()

                al_iterations = self.__data_storage.get_al_optimization_iterations()

                s01number: int = len(al_iterations[1])
                for s01 in range(s01number):  # from 0 to s01number-1
                    i01: int = 0
                    i01number: int = al_iterations[1][s01]
                    while (self.__al_loopers[1]).set_settings(1, s01, i01, i01number) < i01number:

                        i01 += 1

                        s02number: int = len(al_iterations[2])
                        for s02 in range(s02number):  # from 0 to s02number-1
                            i02: int = 0
                            i02number: int = al_iterations[2][s02]
                            while (self.__al_loopers[2]).set_settings(2, s02, i02, i02number) < i02number:

                                i02 += 1

                                # s03number: int = len(al_iterations[3])
                                # for s03 in range(s03number):  # from 0 to s03number-1
                                #     i03: int = 0
                                #     i03number: int = al_iterations[3][s03]
                                #     while (self.__al_loopers[3]).set_settings(3, s03, i03, i03number) < i03number:
                                #
                                #         i03 += 1
                                #
                                #         self.__data_storage.init_results_collection()
                                #
                                #         s04number: int = len(al_iterations[4])
                                #         for s04 in range(s04number):  # from 0 to s04number-1
                                #             i04: int = 0
                                #             i04number: int = al_iterations[4][s04]
                                #             while (self.__al_loopers[4]).set_settings(4, s04, i04, i04number) < i04number:
                                #
                                #                 i04 += 1
                                #
                                #                 s05number: int = len(al_iterations[5])
                                #                 for s05 in range(s05number):  # from 0 to s05number-1
                                #                     i05: int = 0
                                #                     i05number: int = al_iterations[5][s05]
                                #                     while (self.__al_loopers[5]).set_settings(5, s05, i05, i05number) < i05number:
                                #
                                #                         i05 += 1
                                #
                                #                         s06number: int = len(al_iterations[6])
                                #                         for s06 in range(s06number):  # from 0 to s06number-1
                                #                             i06: int = 0
                                #                             i06number: int = al_iterations[6][s06]
                                #                             while (self.__al_loopers[6]).set_settings(6, s06, i06, i06number) < i06number:
                                #
                                #                                 i06 += 1
                                #
                                #                                 s07number: int = len(al_iterations[7])
                                #                                 for s07 in range(s07number):  # from 0 to s07number-1
                                #                                     i07: int = 0
                                #                                     i07number: int = al_iterations[7][s07]
                                #                                     while (self.__al_loopers[7]).set_settings(7, s07, i07, i07number) < i07number:
                                #
                                #                                         i07 += 1
                                #
                                #                                         s08number: int = len(al_iterations[8])
                                #                                         for s08 in range(s08number):  # from 0 to s08number-1
                                #                                             i08: int = 0
                                #                                             i08number: int = al_iterations[8][s08]
                                #                                             while (self.__al_loopers[8]).set_settings(8, s08, i08, i08number) < i08number:
                                #
                                #                                                 i08 += 1
                                #
                                #                                                 s09number: int = len(al_iterations[9])
                                #                                                 for s09 in range(s09number):  # from 0 to s09number-1
                                #                                                     i09: int = 0
                                #                                                     i09number: int = al_iterations[9][s09]
                                #                                                     while (self.__al_loopers[9]).set_settings(9, s09, i09, i09number) < i09number:
                                #
                                #                                                         i09 += 1
                                #
                                #                                                         s10number: int = len(al_iterations[10])
                                #                                                         for s10 in range(s10number):  # from 0 to s10number-1
                                #                                                             i10: int = 0
                                #                                                             i10number: int = al_iterations[10][s10]
                                #                                                             while (self.__al_loopers[10]).set_settings(10, s10, i10, i10number) < i10number:
                                #
                                #                                                                 i10 += 1
                                #
                                #                                                                 s11number: int = len(al_iterations[11])
                                #                                                                 for s11 in range(s11number):  # from 0 to s11number-1
                                #                                                                     i11: int = 0
                                #                                                                     i11number: int = al_iterations[11][s11]
                                #                                                                     while (self.__al_loopers[11]).set_settings(11, s11, i11, i11number) < i11number:
                                #
                                #                                                                         i11 += 1
                                #
                                #                                                                         s12number: int = len(al_iterations[12])
                                #                                                                         for s12 in range(s12number):  # from 0 to s12number-1
                                #                                                                             i12: int = 0
                                #                                                                             i12number: int = al_iterations[12][s12]
                                #                                                                             while (self.__al_loopers[12]).set_settings(12, s12, i12, i12number) < i12number:
                                #
                                #                                                                                 i12 += 1
                                #
                                #                                                                                 self.__trigger_training()

    def __init_static_loopers(self) -> None:

        if isinstance(self.__container.get('fixed_loopers_markers'), list):

            as_loopers_markers: list[str] = self.__container.get('fixed_loopers_markers')
            self.__fixed_loopers_number = len(as_loopers_markers)

            for i_looper in range(self.__fixed_loopers_number):
                self.__al_loopers[i_looper] = self.__container.get(as_loopers_markers[i_looper])
                (self.__al_loopers[i_looper]).init(i_looper)

    def __init_dynamic_loopers(self) -> None:
        pass

    def __trigger_training(self) -> None:
        pass
