from DI.DIContainer import DIContainer
from DataStorage.DataStorage import DataStorage
from Network.NetworkProcessor import NetworkProcessor
from Network.NetworkResultsCollector import NetworkResultsCollector
from Network.Input.InputCollector import InputCollector
from Sequencer.Sequencer import Sequencer


def set_sequencer(container: DIContainer) -> Sequencer:
    return Sequencer(
        container.get(DataStorage.__module__),
        container.get(InputCollector.__module__),
        container.get(NetworkProcessor.__module__),
        container.get(NetworkResultsCollector.__module__),
        container
    )


container_sequencer_mapper = {
    Sequencer.__module__: set_sequencer
}
