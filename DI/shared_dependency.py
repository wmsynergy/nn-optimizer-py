from DI.DIContainer import DIContainer
from DataStorage.DataStorage import DataStorage


def set_data_storage(container: DIContainer) -> DataStorage:
    return DataStorage()


container_shared_mapper = {
    DataStorage.__module__: set_data_storage
}
