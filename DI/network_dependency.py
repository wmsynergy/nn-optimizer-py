from DI.DIContainer import DIContainer
from Network.Input.InputCollector import InputCollector
from Network.NetworkProcessor import NetworkProcessor
from Network.NetworkResultsCollector import NetworkResultsCollector


def set_input_collector(container: DIContainer) -> InputCollector:
    return InputCollector()

def set_network_processor(container: DIContainer) -> NetworkProcessor:
    return NetworkProcessor()

def set_network_results_collector(container: DIContainer) -> NetworkResultsCollector:
    return NetworkResultsCollector()


container_network_mapper = {
    InputCollector.__module__: set_input_collector,
    NetworkProcessor.__module__: set_network_processor,
    NetworkResultsCollector.__module__: set_network_results_collector
}
